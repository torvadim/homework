
# Readme

## How to run project

1. Clone repository
2. Navigate to repository (`cd homework`)
3. Install `node.js` if needed (https://nodejs.org/en/download/)
4. Run `npm i`
5. Run `npm run start`

Service will be running locally on port 3000. 

Server is accessible on `http://localhost:3000`

## How to run test

Run `npm run test`

## Endpoints

### 1. GET `/`

Placeholder, returns some text.

***

### 2. GET `/status`

Returns state of the application and how long is it running.

***

### 3. GET `/card-expiration-date/{card number}`

**This endpoint is protected by Basic Auth**

**Credentials:** 

login: `admin` 

password: `12345Ab+` 


Returns date of the expiration and status of the card. 
