import express from 'express'
import fetch from "node-fetch"
import dayjs from 'dayjs'

import authMiddleware from './auth.js'

const app = express()

app.get('/', (req, res) => {
    res.send('Hi there! This is the base route for this application. For more info please read README.md.')
  })

app.get('/status', (req, res) => {
  res.send(`Server is running for ${process.uptime()} seconds`)
})

app.use(authMiddleware)

app.get('/card-expiration-date/:id', async (req, res) => {
    const cardNumber = req.params.id


    const [cardExpirationResponse, cardDataResponse] = await Promise.all([ 
        fetch(`http://private-264465-litackaapi.apiary-mock.com/cards/${cardNumber}/validity`), 
        fetch(`http://private-264465-litackaapi.apiary-mock.com/cards/${cardNumber}/state`)
    ])


    const [cardExpirationData, cardData] = await Promise.all([cardExpirationResponse.json(), cardDataResponse.json()])

    const formattedExpirationDate = dayjs(cardExpirationData.validity_end).format('DD.MM.YYYY').toString()

    res.send(`${formattedExpirationDate}, ${cardData.state_description}`)
  })
  

export default app