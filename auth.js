const authMiddleware = (request, response, next) => {
    const authHeader = (request.headers.authorization || '').split(' ')[1] || ''

    const [login, password] = Buffer.from(authHeader, 'base64').toString().split(':')

    if (login === 'admin' && password === '12345Ab+') {
        return next()
    }

    response.status(401).send('Authentication required.')
}


export default authMiddleware