import request from 'supertest'

import app from './app.js'


describe("Test the root path", () => {
    test("GET / return 200", async () => {
        const response = await request(app).get("/")
        expect(response.statusCode).toBe(200)
    })
})

describe("Test the status", () => {
    test("GET /status return 200 and have body", async () => {
        const response = await request(app).get("/status")
        expect(response.statusCode).toBe(200)
        expect(response.body).toBeTruthy()
    })
})

describe("Test if auth works", () => {
    test("GET /card-expiration-date/:id return 401 without auth header", async () => {
        const response = await request(app).get("/card-expiration-date/4500444455556666")
        expect(response.statusCode).toBe(401)
        expect(response.error.text).toBe("Authentication required.")
    })
})

describe("Test card status", () => {
    test("GET /card-expiration-date/:id return 401 without auth header", async () => {
        const response = await request(app)
            .get("/card-expiration-date/4500444455556666")
            .auth('admin', '12345Ab+')
            
        expect(response.statusCode).toBe(200)
        expect(response.body).toBeTruthy()
    })
})

